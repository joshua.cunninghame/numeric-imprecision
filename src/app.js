'use strict';

// requirements
const Money = require('./money').Money;
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main
app.get('/', (req, res) => {
    const amount = req.query.amount;
    const { total, tax } = getTaxedTotal(amount);
    res.status(200).end('The total amount including tax is: ' + parseFloat(total))
});

// return total amount with tax
var getTaxedTotal = (value) => {
    const amount = new Money(value)
    const {total, tax} = amount.applyTax(0.10)
    return {
        total: total.asFloat(),
        tax: tax.asFloat()
    }
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, getTaxedTotal };
