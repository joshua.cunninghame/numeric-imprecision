const Big = require('big.js')

// 16 figures + 2 decimal places + decimal point
const MAX_MONEY_LENGTH = 19

class Money {

  constructor(value) {
    if (!value) {
      throw new TypeError('Money value cannot be undefined or empty')
    } else if (!isString(value)) {
      throw new TypeError('Money value must of type string')
    } else if (value.length > MAX_MONEY_LENGTH) {
      throw RangeError(`Amount value must be less than 19 significant digits`)
    }

    const bigValue = toBig(value);
    if (inRangeIncBig(bigValue)(0, Number.MAX_SAFE_INTEGER)) {
      throw RangeError(`Amount must be a positive value less than ${Number.MAX_SAFE_INTEGER}`)
    }

    this._value = bigValue;
    Object.freeze(this)
  }

  applyTax(value) {
    const tax_modifier = new Big(value);
    const tax = this._value.times(tax_modifier)
    const total = this._value.plus(tax)
    return {
      total: new Money(total.toFixed(2)),
      tax: new Money(tax.toFixed(2))
    }
  }

  asFloat() {
    return parseFloat(this._value.toFixed(2));
  }

  asBig() {
    return this._value;
  }

  asString() {
    return this._value.toFixed(2);
  }
}

function toBig(value) {
  try {
    return new Big(value)
  } catch (err) {
    throw RangeError(`Amount must be a positive value less than ${Number.MAX_SAFE_INTEGER}`)
  }
}

function isString(value) {
  return typeof value === 'string' || value instanceof String
}

function inRangeIncBig(amount) {
  return (min, max) => amount.lt(min) || amount.gt(max)
}

module.exports = {Money};
