const request = require('supertest');
const {app, getTaxedTotal} = require('./app');

describe('security', () => {

  it('Request to -500, should throw RangeError exception', async () => {
    expect.assertions(1);
    try {
      getTaxedTotal('-500');
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it('Request to -Infinity, should throw RangeError exception', async () => {
    expect.assertions(1);
    try {
      getTaxedTotal('-Infinity');
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it('Request to Infinity, should throw RangeError exception', async () => {
    expect.assertions(1);
    try {
      getTaxedTotal('Infinity');
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it('Request to undefined, should throw TypeError exception', async () => {
    expect.assertions(1);
    try {
      getTaxedTotal(undefined);
    } catch (ex) {
      expect(ex).toBeInstanceOf(TypeError);
    }
  });

  it('Request to blank, should throw TypeError exception', async () => {
    expect.assertions(1);
    try {
      getTaxedTotal("");
    } catch (ex) {
      expect(ex).toBeInstanceOf(TypeError);
    }
  });

  it('1000000.1 amount, should return 1100000.11 total and 100000.01 tax', async () => {

    const {total, tax} = getTaxedTotal("1000000.1");
    expect(total).toBe(1100000.11);
    expect(tax).toBe(100000.01);
  });

  it('0.15 amount, should return 0.17 total and 0.02 tax', async () => {

    const {total, tax} = getTaxedTotal("0.15");
    expect(total).toBe(0.17);
    expect(tax).toBe(0.02);
  });

  it('Number.MAX_SAFE_INTEGER+1 and Number.MAX_SAFE_INTEGER+2, should not return same total and tax and throw RangeError', async () => {
    expect.assertions(1);
    try {
      const {total: total1, tax: tax1} = getTaxedTotal("9007199254740992");
      const {total: total2, tax: tax2} = getTaxedTotal("9007199254740993");
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

  it('Number.MAX_SAFE_INTEGER-1 should return range error once a 10% tax is applied', async () => {
    expect.assertions(1);
    try {
      const {total: total, tax: tax} = getTaxedTotal("9007199254740990");
    } catch (ex) {
      expect(ex).toBeInstanceOf(RangeError);
    }
  });

});
